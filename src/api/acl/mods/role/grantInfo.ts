/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc grantInfo
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export class GrantInfoParams {
  constructor(public mids?: Array<number>) {}
}

export default async function (
  id: number,

  params: GrantInfoParams,
  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: Array<acl.ModuleInfo>;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "get",
    url: `/role/${id}/grant/info`,

    params,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
