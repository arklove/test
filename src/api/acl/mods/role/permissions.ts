/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc permissions
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  id: number,

  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: acl.PermissionInfo;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "get",
    url: `/role/${id}/permissions`,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
