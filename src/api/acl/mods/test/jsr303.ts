/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc jsr303
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  idCard: acl.TestIdCard,

  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: void;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "post",
    url: `/jsr303`,
    data: idCard,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
