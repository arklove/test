/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc login
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  login: acl.Login,

  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: acl.AuthUser;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "post",
    url: `/auth/login`,
    data: login,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
